loggr
=====

### loggr helps you remember what you've done

#### Install

``git clone git@bitbucket.org:JBoy/loggr.git``

``npm install``

``npm run build``

launch (http://127.0.0.1:8110) or create a vhost and start log.

If you want to change port, edit package.json config

#### Contribute

``npm run develop``

launch (http://127.0.0.1:8110) hacking and send pull a request when you're done.