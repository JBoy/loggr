;(function () {
  'use strict'
  angular.module('loggr').directive('myModal', function () {
    return {
      restrict: 'A',
      link: function (scope, element, attr) {
        scope.modalDismiss = function () {
          element.modal('hide')
        }
      }
    }
  })
})()
