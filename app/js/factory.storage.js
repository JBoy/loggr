;(function () {
  'use strict'
  angular.module('loggr').factory('storageFactory', [function () {
    var storageFactory = {
      key: 'loggr'
    }

    storageFactory.init = function () {
      if (window.hasOwnProperty('localStorage')) {
        if (localStorage.getItem(this.key) === null) {
          localStorage.setItem(this.key, JSON.stringify([]))
        }
      } else {
        window.alert('Please considering upgrading browser to be able to use lStore Plugin')
      }
    }

    storageFactory.getAll = function () {
      var lsItems = JSON.parse(localStorage.getItem(this.key))
      return lsItems
    }

    storageFactory.add = function (obj) {
      var lsItems = this.getAll()
      lsItems.push(obj)
      this.writeLS(lsItems)
    }

    storageFactory.addMultiple = function (array) {
      var lsItems = this.getAll()
      for (var i = 0; i < array.length; i++) {
        lsItems.push(array[i])
      }
      this.writeLS(lsItems)
    }

    storageFactory.clear = function () {
      localStorage.removeItem(this.key)
    }

    storageFactory.removeItem = function (key) {
      var lsItems = this.getAll()
      var newData = []
      var iter = 0
      var foundItem = false
      while (iter < lsItems.length) {
        if (key !== lsItems[iter].id) {
          newData.push(lsItems[iter])
        } else {
          foundItem = true
        }
        iter++
      }
      this.writeLS(newData)
      return foundItem
    }

    storageFactory.update = function (obj) {
      var lsItems = this.getAll()
      var iter = 0
      var id = obj.id
      var foundit = false
      while (iter < lsItems.length) {
        if (id === lsItems[iter].id) {
          lsItems[iter] = obj
          foundit = true
        }
        iter++
      }
      this.writeLS(lsItems)
      return foundit
    }

    storageFactory.writeLS = function (array) {
      localStorage.setItem(this.key, JSON.stringify(array))
    }

    storageFactory.itemExists = function (key) {
      var lsItems = this.getAll()
      var iter = 0
      while (iter < lsItems.length) {
        if (key === lsItems[iter].id) {
          return true
        }
        iter++
      }
      return false
    }

    storageFactory.getItem = function (key) {
      var lsItems = this.getAll()
      var iter = 0
      while (iter < lsItems.length) {
        if (key === lsItems[iter].id) {
          return lsItems[iter]
        }
        iter++
      }
      return false
    }

    storageFactory.getNrOfItems = function () {
      return this.getAll().length
    }

    storageFactory.empty = function () {
      localStorage.setItem(this.key, JSON.stringify([]))
    }

    return storageFactory
  }])
})()
