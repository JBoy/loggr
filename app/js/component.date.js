;(function () {
  'use strict'
  angular.module('loggr').component('date', {
    bindings: {
      format: '@',
      date: '='
    },
    controller: function (dateFactory) {
      this.returnStr = ''
      this.date = new Date(this.date)
      switch (this.format.toLowerCase()) {
        case 'yyyy-mm-dd':
          this.returnStr = dateFactory.getYYYYMMDD(this.date)
          break
        case 'w':
          this.returnStr = dateFactory.getWeek(this.date)
          break
        case 'd':
          this.returnStr = dateFactory.getWeekDay(this.date)
          break
      }
    },
    template: '{{ $ctrl.returnStr }}'
  })
})()
