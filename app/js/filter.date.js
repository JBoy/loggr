;(function () {
  'use strict'
  angular.module('loggr').filter('dateFilter', [function () {
    return function (date, format) {
      if (date instanceof Date === false) {
        date = new Date()
      }
      var newFormat = ''
      format = format || 'YYYY-MM-DD'

      switch (format.toLowerCase()) {
        case 'yyyy-mm-dd':
          var yyyy = date.getFullYear().toString()
          var mm = (date.getMonth() + 1).toString()
          var dd = date.getDate().toString()
          newFormat = yyyy + '-' + (mm[1] ? mm : '0' + mm[0]) + '-' + (dd[1] ? dd : '0' + dd[0])
          break
        case 'dddd': // weekday - eg Söndag
          var weekday = new Array(7)
          weekday[0] = 'Söndag'
          weekday[1] = 'Måndag'
          weekday[2] = 'Tisdag'
          weekday[3] = 'Onsdag'
          weekday[4] = 'Torsdag'
          weekday[5] = 'Fredag'
          weekday[6] = 'Lördag'
          newFormat = weekday[date.getDay()]
          break
        case 'wn': // weeknumber
          date = new Date(date.getTime())
          date.setHours(0, 0, 0, 0)
          // Thursday in current week decides the year.
          date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7)
          // January 4 is always in week 1.
          var week1 = new Date(date.getFullYear(), 0, 4)
          // Adjust to Thursday in week 1 and count number of weeks from date to week1.
          newFormat = 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000 - 3 + (week1.getDay() + 6) % 7) / 7)

      }
      return newFormat
    }
  }])
})()
