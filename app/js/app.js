/*global angular */
;(function () {
  'use strict'
  angular.module('loggr', []).controller('mainController', ['$scope', 'dateFactory', 'storageFactory', '$timeout',
    function ($scope, dateFactory, storageFactory, $timeout) {
      storageFactory.init()

      $scope.getLogged = function () {
        var logged = storageFactory.getAll()
        var preppedDateLogs = dateFactory.updateDate(logged)
        $scope.logged = dateFactory.orderByTimestamp(preppedDateLogs)
      }
      $scope.getLogged()

      // Scope vars
      $scope.edited = null
      $scope.class = null
      $scope.today = dateFactory.format('YYYY-MM-DD')
      $scope.origLog = {
        work: null,
        id: null
      }

      /** Scope functions  ****/

      $scope.addNewLog = function () {
        if (!$scope.newLog) {
          return
        }
        if (local.easterEggs[$scope.newLog]) {
          local.easterEggs[$scope.newLog].func()
          $scope.newLog = ''
          return
        }

        var preppedLog = dateFactory.prepareLogForSave({
          work: $scope.newLog
        })
        storageFactory.add(preppedLog)
        $scope.newLog = ''
        local.changeClass()
        $scope.getLogged()
      }

      $scope.editLog = function (log) {
        $scope.origLog = {
          id: log.id,
          work: log.work,
          date: log.date
        }
        $scope.edited = log
        $scope.edited.date = new Date($scope.edited.date)
      }

      $scope.update = function (edited) {
        var log = local.findLogById(edited.id)
        if (log.work === $scope.origLog.work && log.date === $scope.origLog.date) {
          $scope.modalDismiss()
          return
        }

        var result = storageFactory.update(edited)

        if (result) {
          $scope.getLogged()
          local.resetOrig()
          local.changeClass()
        }
        $scope.modalDismiss()
      }

      $scope.cancelEditing = function () {
        var log = local.findLogById($scope.origLog.id)
        log.work = $scope.origLog.work
        local.resetOrig()
      }

      $scope.delete = function () {
        var foundItem = storageFactory.removeItem($scope.origLog.id)
        if (foundItem) {
          var log = local.findLogById($scope.origLog.id)
          local.deleteFromLogged(log)
          local.resetOrig()
          local.changeClass()
        }
      }

      $scope.passNewLog = function (work) {
        $scope.newLog = work
      }

      // local controller vars and func
      var local = {
        easterEggs: {
          randombg: {
            func: function () {
              var color = '#'
              var letters = ['000000', 'FF0000', '00FF00', '0000FF', 'FFFF00', '00FFFF',
                'FF00FF', 'C0C0C0'
              ]

              color += letters[Math.floor(Math.random() * letters.length)]
              document.querySelector('body').style.background = color
              document.querySelector('header').style.background = color
            }
          },
          consolelog: {
            func: function () {
              console.log('%c Green is beautiful and so are you',
                'background: green; color: white; display: block;')
              document.querySelector('.todo-search-field').setAttribute('placeholder',
                'Kolla konsolen')
              setTimeout(function () {
                document.querySelector('.todo-search-field').setAttribute(
                  'placeholder', 'Vad vill du logga?')
              }, 3000)
            }
          }
        },
        findLogById: function (id) {
          for (var i = 0; i < $scope.logged.length; i++) {
            var log = $scope.logged[i]
            if (id === log.id) {
              return log
            }
          }
        },
        resetOrig: function () {
          $scope.origLog.id = null
          $scope.origLog.work = null
        },
        changeClass: function () {
          if (!$scope.class) {
            $scope.class = 'success-animation'
          } else {
            $scope.class = ''
          }
          $timeout(function () {
            $scope.class = ''
          }, 3000)
        },
        deleteFromLogged: function (log) {
          var indexOfLogged = $scope.logged.lastIndexOf(log)
          $scope.logged.splice(indexOfLogged, 1)
        }
      }
    }
  ])
})()
